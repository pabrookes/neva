.EXPORT_ALL_VARIABLES:

.PHONY: lint-markdown
lint-markdown:
	@docker run \
		--rm \
		--interactive \
		--read-only \
		--volume=`pwd`:`pwd` \
		--workdir=`pwd` \
		pipelinecomponents/markdownlint:0.11.0 \
		mdl -c .mdlrc .

.PHONY: lint
lint: lint-markdown

.PHONY: docker-build
docker-build:
	@docker build \
		-t material-for-mkdocs-example:latest \
		.

.PHONY: docs-build
docs-build:
	@docker run --rm \
		--volume=`pwd`:`pwd` \
		--workdir=`pwd` \
		skyross/material-for-mkdocs-example:latest \
		mkdocs build --strict --verbose

.PHONY: docs-serve
docs-serve:
	@docker run --rm \
		--volume=`pwd`:`pwd` \
		--workdir=`pwd` \
		-p 3000:3000 \
		skyross/material-for-mkdocs-example:latest \
		mkdocs serve -a 0.0.0.0:3000
